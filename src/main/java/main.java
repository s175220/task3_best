import java.util.Arrays;
import java.util.Scanner;

public class main {
    public static void main(String[] args){
        System.out.println("Hello World!");

        Scanner scan = new Scanner(System.in);
        String input = scan.next();


        String[] arrOfStr = input.split("[,\\r\\n]+");
        for(String s : arrOfStr){
            System.out.println(s);
        }

        int[] numbersToAdd = Arrays.asList(arrOfStr).stream().mapToInt(Integer::parseInt).toArray();
        System.out.println(add(numbersToAdd));

    }

    public static int add(int[] numbers){
        int sum = 0;
        for(int i : numbers){
            sum += i;
        }
        return sum;
    }
}
